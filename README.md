#Molly Prep

https://github.com/Ada-Developers-Academy/jump-start 

##TECHNICAL

*	~~Variables~~ 
*	~~Data Types~~
	* int
	* float
	* string
*	~~I/O~~
	* stdin/stdout
	* Puts vs Print
*	~~Conditional Statements~~
	* if/elsif/else
	* unless
*	~~Loops~~
	* while
	* for
	* until
	* Escape Conditions/Infinite Loops
*	~~Arrays~~
*	~~Hashmaps/Dictionaries~~
	* Key => Value
	* Access time 
*	~~Code blocks~~
	* Range


##Practical

* Debugging
	* Breakpoints
	* Debug statements
	* Identifying potential causes
	* Trace
* Planning
	* Charting
	* Flow Analysis
* Documentation/Comments
* Using local tools
	* Text editor (Atom or Sublime)
	* Run via Terminal (ruby)
	* Bash (cd, ls, pwd, mkdir, rm, cat, touch, grep)
	* NOTE: GET HOMEBREW

##Big Picture

* HTML, CSS, JS, Ruby, Rails, Framework, SQL
* Compiled vs Interpreted
* Low level vs High Level
* Statically vs Dynamically Typed


##Exercises

* NOTE: Run locally, no repl.it. Post code to Gist.
* Song Play Time (ADA Phase 2 Cohort 6)
* Employee Pay
* IRC Quotes (UCSD CSE Quotes)
 

##Mock Interview

##Post Interview Prep

* git
* Big O
* HTML5
	* DOM
	* Tags
	* Script
* JS <- DO NOT TEACH UNTIL RUBY IS FINISHED
	* See Technical Above

